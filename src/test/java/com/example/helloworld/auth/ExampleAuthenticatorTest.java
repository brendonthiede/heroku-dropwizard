package com.example.helloworld.auth;

import com.example.helloworld.core.User;
import io.dropwizard.auth.basic.BasicAuthProvider;
import io.dropwizard.auth.basic.BasicCredentials;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleAuthenticatorTest {

    @Test
    public void testAuthenticate_goodPassword() throws Exception {
        ExampleAuthenticator exampleAuthenticator = new ExampleAuthenticator();
        User authenticatedUser = exampleAuthenticator.authenticate(new BasicCredentials("anybody", "secret")).get();
        assertEquals("anybody", authenticatedUser.getName());
    }

    @Test
    public void testAuthenticate_badPassword() throws Exception {
        ExampleAuthenticator exampleAuthenticator = new ExampleAuthenticator();
        assertFalse(exampleAuthenticator.authenticate(new BasicCredentials("anybody", "fake secret")).isPresent());
    }
}