package com.example.helloworld.db;

import com.example.helloworld.core.Person;
import org.hibernate.*;
import org.hibernate.internal.QueryImpl;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.stat.SessionStatistics;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.sql.Connection;

public class MockSession implements Session {
    private static final Logger LOGGER = LoggerFactory.getLogger(MockSession.class);

    public MockSession() {
        super();
    }

    @Override
    public void saveOrUpdate(Object object) {
        LOGGER.info("Saving or updating something: " + object.toString());
    }

    @Override
    public void saveOrUpdate(String s, Object object) {
        LOGGER.info("Saving or updating something: " + object.toString());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public void replicate(String s, Object o, ReplicationMode replicationMode) {

    }

    @Override
    public Serializable save(Object o) {
        return null;
    }

    @Override
    public Serializable save(String s, Object o) {
        return null;
    }

    @Override
    public LockRequest buildLockRequest(LockOptions lockOptions) {
        return null;
    }

    @Override
    public void refresh(Object o) {

    }

    @Override
    public void refresh(String s, Object o) {

    }

    @Override
    public void refresh(Object o, LockMode lockMode) {

    }

    @Override
    public void refresh(Object o, LockOptions lockOptions) {

    }

    @Override
    public void refresh(String s, Object o, LockOptions lockOptions) {

    }

    @Override
    public LockMode getCurrentLockMode(Object o) {
        return null;
    }

    @Override
    public void clear() {

    }

    @Override
    public Object get(Class aClass, Serializable serializable) {
        LOGGER.info("Looking for " + aClass.getSimpleName() + " with ID " + serializable.toString());
        return null;
    }

    @Override
    public Object get(Class aClass, Serializable serializable, LockMode lockMode) {
        return null;
    }

    @Override
    public Object get(Class aClass, Serializable serializable, LockOptions lockOptions) {
        return null;
    }

    @Override
    public Object get(String s, Serializable serializable) {
        return null;
    }

    @Override
    public Object get(String s, Serializable serializable, LockMode lockMode) {
        return null;
    }

    @Override
    public Object get(String s, Serializable serializable, LockOptions lockOptions) {
        return null;
    }

    @Override
    public String getEntityName(Object o) {
        return null;
    }

    @Override
    public Filter enableFilter(String s) {
        return null;
    }

    @Override
    public Filter getEnabledFilter(String s) {
        return null;
    }

    @Override
    public void disableFilter(String s) {

    }

    @Override
    public SessionStatistics getStatistics() {
        return null;
    }

    @Override
    public void doWork(Work work) throws HibernateException {

    }

    @Override
    public <T> T doReturningWork(ReturningWork<T> returningWork) throws HibernateException {
        return null;
    }

    @Override
    public Connection disconnect() {
        return null;
    }

    @Override
    public TypeHelper getTypeHelper() {
        return null;
    }

    @Override
    public LobHelper getLobHelper() {
        return null;
    }

    @Override
    public void addEventListeners(SessionEventListener... sessionEventListeners) {

    }

    @Override
    public NaturalIdLoadAccess byNaturalId(String var1) {
        return null;
    }

    @Override
    public NaturalIdLoadAccess byNaturalId(Class var1) {
        return null;
    }

    @Override
    public SimpleNaturalIdLoadAccess bySimpleNaturalId(String var1) {
        return null;
    }

    @Override
    public SimpleNaturalIdLoadAccess bySimpleNaturalId(Class var1) {
        return null;
    }

    @Override
    public void persist(Object var1) {

    }

    @Override
    public void persist(String var1, Object var2) {

    }

    @Override
    public void delete(Object var1) {
        LOGGER.info("Deleting " + var1.toString());
    }

    @Override
    public void delete(String var1, Object var2) {

    }

    @Override
    public void reconnect(Connection var1) {

    }

    @Override
    public void update(Object var1) {

    }

    @Override
    public void update(String var1, Object var2) {

    }

    @Override
    public Object load(Class var1, Serializable var2, LockOptions var3) {
        return null;
    }

    @Override
    public Object load(String var1, Serializable var2, LockMode var3) {
        return null;
    }

    @Override
    public Object load(String var1, Serializable var2, LockOptions var3) {
        return null;
    }

    @Override
    public Object load(Class var1, Serializable var2) {
        return null;
    }

    @Override
    public Object load(String var1, Serializable var2) {
        return null;
    }

    @Override
    public void load(Object var1, Serializable var2) {

    }

    @Override
    public void replicate(Object object, ReplicationMode replicationMode) {

    }

    @Override
    public boolean isReadOnly(Object object) {
        return false;
    }

    @Override
    public void setReadOnly(Object object, boolean isReadyOnly) {

    }

    @Override
    public void enableFetchProfile(String var1) throws UnknownProfileException {

    }

    @Override
    public void disableFetchProfile(String var1) throws UnknownProfileException {

    }

    @Override
    public boolean isFetchProfileEnabled(String var1) throws UnknownProfileException {
        return false;
    }

    @Override
    public Query createFilter(Object var1, String var2) {
        return null;
    }

    @Override
    public SharedSessionBuilder sessionWithOptions() {
        return null;
    }

    @Override
    public void flush() throws HibernateException {

    }

    @Override
    public void setFlushMode(FlushMode flushMode) {

    }

    @Override
    public FlushMode getFlushMode() {
        return null;
    }

    @Override
    public void setCacheMode(CacheMode cacheMode) {

    }

    @Override
    public CacheMode getCacheMode() {
        return null;
    }

    @Override
    public SessionFactory getSessionFactory() {
        return null;
    }

    @Override
    public Connection close() throws HibernateException {
        return null;
    }

    @Override
    public void cancelQuery() throws HibernateException {

    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public boolean isDirty() throws HibernateException {
        return false;
    }

    @Override
    public boolean isDefaultReadOnly() {
        return false;
    }

    @Override
    public void setDefaultReadOnly(boolean b) {

    }

    @Override
    public Serializable getIdentifier(Object o) {
        return null;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public void evict(Object o) {

    }

    @Override
    public Object load(Class aClass, Serializable serializable, LockMode lockMode) {
        return null;
    }

    @Override
    public String getTenantIdentifier() {
        return null;
    }

    @Override
    public Transaction beginTransaction() {
        return null;
    }

    @Override
    public Transaction getTransaction() {
        return null;
    }

    @Override
    public Query getNamedQuery(String var1) {
        LOGGER.info("Using named query: " + var1);
        return new MockQuery();
    }

    @Override
    public Query createQuery(String var1) {
        return null;
    }

    @Override
    public SQLQuery createSQLQuery(String var1) {
        return null;
    }

    @Override
    public ProcedureCall getNamedProcedureCall(String var1) {
        return null;
    }

    @Override
    public ProcedureCall createStoredProcedureCall(String var1) {
        return null;
    }

    @Override
    public ProcedureCall createStoredProcedureCall(String var1, Class... var2) {
        return null;
    }

    @Override
    public ProcedureCall createStoredProcedureCall(String var1, String... var2) {
        return null;
    }

    @Override
    public Criteria createCriteria(Class var1) {
        return null;
    }

    @Override
    public Criteria createCriteria(Class var1, String var2) {
        return null;
    }

    @Override
    public Criteria createCriteria(String var1) {
        return null;
    }

    @Override
    public Criteria createCriteria(String var1, String var2) {
        return null;
    }

    @Override
    public void lock(Object var1, LockMode var2) {

    }

    @Override
    public void lock(String var1, Object var2, LockMode var3) {

    }

    @Override
    public IdentifierLoadAccess byId(String var1) {
        return null;
    }

    @Override
    public IdentifierLoadAccess byId(Class var1) {
        return null;
    }

    @Override
    public Object merge(Object var1) {
        return null;
    }

    @Override
    public Object merge(String var1, Object var2) {
        return null;
    }
}
