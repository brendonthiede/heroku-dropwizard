package com.example.helloworld.utils;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.LogbackException;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import ch.qos.logback.core.status.Status;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class LogMonitor {
    TestableAppender testableAppender;
    Logger logger;

    public LogMonitor(Class logClass) {
        testableAppender = new TestableAppender();

        logger = (Logger) LoggerFactory.getLogger(logClass.getName());
        // noinspection unchecked
        logger.addAppender(testableAppender);
        logger.setLevel(Level.DEBUG);
        logger.setAdditive(true);
    }

    public ArrayList<String> getMessages() {
        return testableAppender.getMessages();
    }

    class TestableAppender implements Appender {
        public ArrayList<String> messages;

        public TestableAppender() {
            messages = new ArrayList<>();
        }

        public ArrayList<String> getMessages() {
            return messages;
        }

        @Override
        public void doAppend(Object messageEntry) throws LogbackException {
            messages.add(messageEntry.toString());
        }

        @Override
        public String getName() {
            return null;
        }

        @Override
        public void setName(String name) {

        }

        @Override
        public void setContext(Context context) {

        }

        @Override
        public Context getContext() {
            return null;
        }

        @Override
        public void addStatus(Status status) {

        }

        @Override
        public void addInfo(String s) {

        }

        @Override
        public void addInfo(String s, Throwable throwable) {

        }

        @Override
        public void addWarn(String s) {

        }

        @Override
        public void addWarn(String s, Throwable throwable) {

        }

        @Override
        public void addError(String s) {

        }

        @Override
        public void addError(String s, Throwable throwable) {

        }

        @Override
        public void addFilter(Filter filter) {

        }

        @Override
        public void clearAllFilters() {

        }

        @Override
        public List<Filter> getCopyOfAttachedFiltersList() {
            return null;
        }

        @Override
        public FilterReply getFilterChainDecision(Object o) {
            return null;
        }

        @Override
        public void start() {

        }

        @Override
        public void stop() {

        }

        @Override
        public boolean isStarted() {
            return false;
        }
    }

}

