package com.example.helloworld.resources;

import com.example.helloworld.core.Saying;
import com.example.helloworld.core.Template;
import com.example.helloworld.utils.LogMonitor;
import com.google.common.base.Optional;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HelloWorldResourceTest {
    private HelloWorldResource resource;
    private static final String DEFAULT_NAME = "Default Name";

    @Before
    public void setup() {
        resource = new HelloWorldResource(new Template("Hello, %s!", DEFAULT_NAME));
    }

    @Test
    public void testSayHelloAddsNameGivenToTemplate() throws Exception {
        Saying saying = resource.sayHello(Optional.fromNullable("My Name"));
        assertEquals("Hello, My Name!", saying.getContent());
        assertEquals(1, saying.getId());
    }

    @Test
    public void testSayHelloIncrementsIdEachCall() throws Exception {
        Saying saying = resource.sayHello(Optional.fromNullable("My Name"));
        assertEquals("Hello, My Name!", saying.getContent());
        assertEquals("First call to new instance should have ID 1", 1, saying.getId());
        saying = resource.sayHello(Optional.fromNullable("Dude"));
        assertEquals("Hello, Dude!", saying.getContent());
        assertEquals("Saying hello again should increment ID", 2, saying.getId());
        saying = resource.sayHello(Optional.fromNullable("That Guy"));
        assertEquals("Hello, That Guy!", saying.getContent());
        assertEquals("Saying hello again should increment ID", 3, saying.getId());
    }

    @Test
    public void testSayHelloUsesDefaultName() throws Exception {
        final Optional<String> NAME = Optional.absent();
        assertEquals("Hello, " + DEFAULT_NAME + "!", resource.sayHello(NAME).getContent());
    }

    @Test
    public void testReceiveHello() throws Exception {
        LogMonitor logMonitor = new LogMonitor(HelloWorldResource.class);
        resource.receiveHello(new Saying(1, "Hello?"));
        assertEquals("[INFO] Received a saying: Hello?", logMonitor.getMessages().get(0));
    }
}