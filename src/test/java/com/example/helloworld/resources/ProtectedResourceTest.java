package com.example.helloworld.resources;

import com.example.helloworld.core.User;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProtectedResourceTest {
    private static final ProtectedResource resource = new ProtectedResource();

    @Test
    public void testShowSecret() throws Exception {
        assertEquals("Hey there, Bob. You know the secret!", resource.showSecret(new User("Bob")));
    }
}